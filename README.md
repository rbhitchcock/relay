# Getting Started

## Installing relay on CentOS/Redhat
* First install Docker and Docker Compose \(below shows an easy way to install both\)

```
$> tee /etc/yum.repos.d/docker.repo <<-EOF
     [dockerrepo]
     name=Docker Repository
     baseurl=https://yum.dockerproject.org/repo/main/centos/7
     enabled=1
     gpgcheck=1
     gpgkey=https://yum.dockerproject.org/gpg
   EOF

$> yum -y install docker-engine
$> systemctl start docker
$> curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/bin/docker-compose
$> chmod +x /usr/bin/docker-compose
```
* After installing Docker and Docker Compose, install the relay RPM:

```
$> rpm -i relay.rpm
```

* After the relay RPM has been installed, follow the directions printed out from the RPM installation process and update the following file:

```
$> vi /etc/norad.d/configuration.yml
```

* To have norad-relay start on boot:

```
$> systemctl enable norad-relay.service
```

### Upgrading norad-relay RPM from 1.0

The 1.0 norad-relay RPM contained an errant postrm script to remove configuration files.  Unfortunately, the removal scripts impacts the typcial upgrade using rpm -U.  The safest option for upgrade is to run the below command:

```
 rpm -Uvh --nopostun norad-relay-1.0.2-x86_64.rpm
```
Versions 1.0.1 and after have fixed this issue and do not require the --nopostun flag.

## Installing relay on Ubuntu 14.04
* First install Docker and Docker Compose \(below shows an easy way to install both\)

```
$> apt-get -y update
$> apt-get -y install apt-transport-https ca-certificates
$> apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
$> echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" >> /etc/apt/sources.list.d/docker.list
$> apt-get -y update
$> apt-get -y purge lxc-docker
$> apt-get -y install linux-image-extra-$(uname -r) docker-engine
$> curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/bin/docker-compose
$> chmod +x /usr/bin/docker-compose
```
* After installing Docker and Docker Compose, install the relay DEB:

```
$> dpkg -i norad-relay-1.0.x-amd64.deb
```

* To have norad-relay start on reboot:

```
$> update-rc.d norad-relay defaults
$> update-rc.d norad-relay enable
```

### Upgrading norad-relay from DEB 1.0.0

The 1.0.0 norad-relay DEB contained an errant postrm script to remove configuration files.  Unfortunately, the removal scripts impacts the typcial upgrade using dpkg -i.  The safest option for upgrade is to run the below commands (as root):

```
rm /var/lib/dpkg/info/norad-relay.postrm
dpkg -i norad-relay-1.0.x-amd64.deb
```

Versions 1.0.1 and after have fixed this issue and do not require the above rm.

## References
* [Official Docker Engine Installation Instructions](https://docs.docker.com/engine/installation/)
* [Official Docker Compose Installation Instructions](https://docs.docker.com/compose/install/)

# Connectivity Requirements

For installations behind a firewall, the below rules must be added to enable relay communication with the Norad cloud:

| IP            | Port | Protocol | Reason                       |
|---------------|------|----------|------------------------------|
| 128.107.33.76 | 8443 | TCP      | API                          |
| 128.107.33.76 | 5671 | TCP      | Rabbitmq                     |
| 128.107.33.78 | 5000 | TCP      | Docker Registry Secrvice     |
| 128.107.33.78 | 5001 | TCP      | Docker Registry Auth Service |

# Usage

## Configuring the Relay

The configuration file for relay is located at `/etc/norad.d/configuration.yml`

The following options are available for configuration:

| Config Option            | Description                                             | Default Value                   |
|--------------------------|---------------------------------------------------------|---------------------------------|
| api_uri                  | URL to Norad Cloud or Norad Enterprise                  | https://norad.cisco.com:8443/v1 |
| organization_token       | Org token used to register relay with organization      | None                            |
| worker_pool_size         | # of concurrent containers allowed to run on this relay | 4                               |
| relay_host_docker_socket | Socket used for relay to communicate with docker daemon | unix:///relay/docker.sock       |

## Running the Relay

If this is the first time you are running the Relay, it will attempt to register itself in the API.
In order for jobs to be routed to your Relay, you must approve the new Relay once it is registered.
To approve the Relay, either visit the Organization Settings configuration page or you can query the
[API directly](https://norad.cisco.com:8443/docs/v1/docker_relays/update.html). You can also
configure your Organization to always auto-approve any new relays on the Organization Settings
configuration page or directly in the API using the [Organization Configurations
endpoint](https://norad.cisco.com:8443/docs/v1/organization_configurations.html).

### CentOS/Redhat
* To start the relay:

```
$> systemctl start norad-relay.service
```

* To stop the relay:

```
$> systemctl stop norad-relay.service
```

* To check the relay's running status

```
$> systemctl status norad-relay.service
```

* For more verbose output

```
$> journalctl -u norad-relay.service
```

### Ubuntu
* To start the relay:

```
$> /etc/init.d/norad-relay start
```

* To stop the relay:

```
$> /etc/init.d/norad-relay stop
```

## Updating Container Options

When the Relay starts, it will check for the existence of a plaintext container configuration file
(`/relay/containers.yml` from within the container by default). If found, Relay will encrypt the
contents using the public key generated by the API during Relay registration, save a new file with a
`.enc` extension, and remove the plaintext file.

If you need to change the container configuration options, simply create a new file called
`/etc/norad.d/containers.yml`, add your options, and restart the Relay container.

### Container configuration
Container configuration works by creating a containers.yml file that specifies the options you would
like to override. Configuring an override also requires that properly format the configuration in
the API as well.

The first thing you'll need to do is create your YAML file. You can specify multiple containers in
the same file. Be sure to use the full Security Container name (including tag) for each section.

```yaml
norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1:
  custom_port: 9999
```

Security configurations will need to be updated in the UI or API to use the override values on the
relay.  The key used in the containers.yml file must match the security container setting specified
in the UI or API.  For example, in the above containers.yml, a ```custom_port``` is set for the
nmap-ssl-dh-param security container.  To have the relay's configuration override the Norad cloud
settings, you are required to change nmap-ssl-dh-param's port configuration setting to
```%{custom_port}``` in the UI or API.  For the UI, go to either the Organization Settings or the
Machine Settings.  For the API, refer to the API documentation at
https://norad.cisco.com:8443/docs/v1/security_container_configs.html. Note that the value of the
config matches the name of the option you wish to override in the YAML file. This name is arbitrary;
it is up to you to pick the name. Just be sure that they match.

Names must begin with an uppercase letter, lowercase letter, or underscore. Any subsequent
characters are optional, but must be an upperase letter, lowercass letter, underscore, or digit. In
other words, they must satisfy the following regular expression: ```\A[a-zA-Z_][a-zA-Z0-9_]\z```.

### SSH Key Configuration
SSH Key configuration is a special case due to the way the SSH user and key arguments are built on
the API backend. Because of this, you must follow more specific guidelines for adding SSH key
arguments to the containers.yml file. Specifically, if you wish to use a local key value for a given
container, you **must** set the ```ssh_user``` and the ```ssh_key``` option for that container.

```yaml
norad-registry.cisco.com:5000/serverspec:0.0.1:
  ssh_user: test-user
  ssh_key: <base64 encoded key>
```

The other important note is that the value for ```ssh_key``` must be a Base64 encoded SSH key.
not contain any line-feeds. In Ubuntu (and likely other *nix systems) this value can be generated
using the ```base64``` utility.

```bash
$> base64 my_key.pem
```

## Debugging

### Logging
* For debugging, logs can be viewed by doing:

```
$> docker logs -f norad_relay
```

### Relay Mode
* By default the Norad Relay starts in production mode.  This means that after running a security container on the relay host,
the container will be deleted for security purposes.  There may be a time when a given container needs to remain intact for
debugging purposes.  To get this behavior, the Norad Relay should be started in debug mode.  To start the relay in debug mode
follow these directions:

    * SSH to the host where your Relay is running
    * In a text editor, as root, open `/etc/norad.d/relay.yml`
    * Under the `relay:` object add the following line:

    ```yaml
    environment:
        - RELAY_MODE=debug
    ```

    * Your config may be slightly different, but it should look similar to the config below when done:


    ```yaml
    relay:
      restart: always
      container_name: norad_relay
      image: norad-registry.cisco.com:5000/relay:latest
      volumes:
        - /etc/norad.d:/relay:rw
        - /var/run/docker.sock:/relay/docker.sock
      environment:
        - RELAY_MODE=debug
    ```

    * After editing and saving `/etc/norad.d/relay.yml` restart the Norad Relay
        * Ubuntu: `sudo service norad-relay restart`
        * RHEL/CentOS: `sudo systemctl restart norad-relay.service`

    * To put the Relay back in to production mode simply change the word `debug` to `production` and restart once again.
