# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'relay/version'

Gem::Specification.new do |spec|
  spec.name          = 'relay'
  spec.version       = Relay::VERSION
  spec.authors       = ['Blake Hitchcock', 'Brian Manifold', 'Roger Seagle']
  spec.email         = ['blhitchc@cisco.com', 'bmanifol@cisco.com', 'rseagle@cisco.com']

  spec.license       = 'Apache-2.0'
  spec.summary       = ''
  spec.description   = ''
  spec.homepage      = 'https://norad-gitlab.cisco.com/norad/relay'
  spec.required_ruby_version = ' ~> 2.3.0'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)
  spec.metadata['allowed_push_host'] = 'http://asig.cisco.com'

  spec.files = Dir.glob('{bin,lib}/**/*') + %w(README.md Gemfile Gemfile.lock Rakefile relay.gemspec)
  spec.files = spec.files + Dir['vendor/**/*'] + Dir['vendor/**/.*/*'] + Dir['.bundle/*']
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'rspec', '~> 3.4.0'
  spec.add_development_dependency 'yard', '~> 0.8.7'
  spec.add_development_dependency 'rubocop', '~> 0.37.2'

  spec.add_runtime_dependency 'bundler'
  spec.add_runtime_dependency 'rake', '~> 11.2.2'
  spec.add_runtime_dependency 'safe_yaml', '~> 1.0.4'
  spec.add_runtime_dependency 'bunny', '~> 2.3.0'
  spec.add_runtime_dependency 'log4r', '~> 1.1.10'
  spec.add_runtime_dependency 'rest-client', '~> 1.8.0'
  spec.add_runtime_dependency 'activesupport', '~> 4.2.5'
  spec.add_runtime_dependency 'docker-api', '~> 1.26.2'
end
