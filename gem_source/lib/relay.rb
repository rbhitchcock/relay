require 'relay/relayer'
require 'relay/configuration'
require 'relay/crypto_utilities'

# Relay faciliates communication between the Norad cloud and one or more hosts in private network.
# Relay creates an outbound connection to an AMQP server running in the Norad cloud and waits for
# instructions. When a command is received, Relay performs sanity checking on the message and then
# sends requests to a Docker daemon to initiate Norad scanners within the private network.
module Relay
end
