require 'ext/json_store'

module Relay
  # A wrapper for a JSON Store
  class Store
    RELAY_META_INFO = {
      'api_db_relay_id' => Integer,
      'queue_name' => String
    }.freeze

    def initialize
      @db = JSON::Store.new Configuration.values[:db], true # Pass true to make the PStore thread safe
      @db.ultra_safe = true
    end

    def to_hash
      @db.transaction(true) do
        @db.roots.each_with_object({}) do |root, h|
          next if root == 'meta'
          h[root] = @db[root]
        end
      end
    end

    # FIXME: Refactor this to work with the other update method
    def update_meta(key, value)
      @db.transaction do
        @db['meta'] ||= {}
        @db['meta'][key.to_s] = value
      end
    end

    def fetch_meta(key)
      @db.transaction do
        @db['meta'] ||= {}
        @db['meta'][key.to_s]
      end
    end
  end
end
