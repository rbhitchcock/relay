require 'relay/logger'
require 'docker-api'

module Relay
  # Provides an interface to the Docker socket running on the host
  class DockerDaemon
    include Relay::Logger
    CONTAINERS_FILE = '/etc/norad.d/containers.yml.enc'.freeze

    # Amount of time container is allowed to run. Currently set to 3 Hours
    CONTAINER_TIMEOUT = 24 * (60 * 60)

    def initialize
      Docker.url = Configuration.values[:relay_host_docker_socket]
    end

    # Sends a message to the Docker socket
    #
    # param message [Hash] message to send to the Docker socket
    def send(msg)
      pull_image(msg['Image'])
      container = start_container(msg)
      log_container_errors(container)
    rescue StandardError => e
      handle_exception(e)
    ensure
      remove_container(container) if ENV['RELAY_MODE'] == 'production'
    end

    private

    def start_container(msg)
      logger.debug "Starting container for image: #{msg['Image']}..."
      container = Docker::Container.create container_opts(msg)
      container.start
      logger.debug 'Started.'
      container.wait(CONTAINER_TIMEOUT)
      logger.debug 'Container exited.'
      container
    end

    def remove_container(container)
      logger.debug 'Removing container...'
      container.delete(force: true) if container
      logger.debug 'Container removed.'
    end

    def log_container_errors(container)
      container.refresh!
      return no_container_info_error(container) unless container.info.is_a?(Hash)
      if container.info.dig('State', 'ExitCode').to_s !~ /\A0\z/
        logger.error 'Container did not exit gracefully. Logs below:'
        logger.error container.streaming_logs(stdout: true, stderr: true)
      end
    end

    def no_container_info_error(container)
      logger.error 'Unable to retrieve container exit code and logs!'
      logger.error "Container info: #{container.info.inspect}"
    end

    # Call this method before starting a container in order to ensure that the node has the latest
    # version of the image you are wanting to run
    def pull_image(image)
      logger.debug "Pulling image: #{image}"
      # Analogous to `docker pull <image_name>`
      Docker::Image.create fromImage: image
    end

    def container_opts(msg)
      aes_key = Base64.strict_encode64(CryptoUtilities.retrieve_aes_key(msg.delete('encoded_relay_private_key')))
      {
        Image: msg['Image'],
        Env: msg['Env'] + ["NORAD_SECURITY_CONTAINER_NAME=#{msg['Image']}", "NORAD_CONTAINERS_FILE_KEY=#{aes_key}"],
        Cmd: msg['Cmd']
      }.tap do |h|
        h[:HostConfig] = { Binds: ["#{CONTAINERS_FILE}:/containers.yml.enc:ro"] } if File.exist?(CONTAINERS_FILE)
      end
    end

    def handle_exception(e)
      logger.error 'An uncaught exception occurred while attempting to run a test'
      logger.error e.message
      logger.error e.backtrace
    end
  end
end
