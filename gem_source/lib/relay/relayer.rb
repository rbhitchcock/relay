require 'relay/docker_queue'
require 'relay/docker_daemon'
require 'relay/message_validator'
require 'relay/api'

module Relay
  # Provide an interface for setting up a connection between a [Relay::Listener] and a
  # [Relay::DockerConnection]
  class Relayer
    # Create an instance of the Relayer class. Peforms registration with the Norad cloud if the
    # Relay datastore is empty.
    def initialize
      @api = Api.new
      CryptoUtilities.encrypt_containers_file(@api)
      @queue = DockerQueue.new @api.queue_name
      @daemon = DockerDaemon.new
    end

    # Leverage an instace of [Relay::Listener] to wait for messages on the queue
    def listen
      @queue.subscribe do |msg|
        relay msg['docker_options'] if MessageValidator.valid?(msg)
      end
    end

    # Send the received message to [Relay::Docker] for further processing
    #
    # @param message [Hash] the message to relay
    # @return [Hash] meta info from Docker
    def relay(message)
      @daemon.send message
    end
  end
end
