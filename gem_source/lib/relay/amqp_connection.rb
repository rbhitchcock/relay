require 'bunny'
require 'uri'
require 'relay/logger'

module Relay
  # Creates and encapsulates the connection to the AMQP server
  class AmqpConnection
    include Relay::Logger

    attr_reader :queue

    # Establishes a connection with the AMQP server
    #
    # @param queue_name [String] name of the queue to connect to
    def initialize(queue_name)
      setup_rabbitmq_connection(queue_name)
    end

    # Closes the channel and the connection
    def close
      @channel.close
      @server.close
    end

    private

    def setup_rabbitmq_connection(queue_name)
      connect_to_server
      create_channel
      create_queue(queue_name)
    end

    def connect_to_server
      @server = Bunny.new(
        host: URI.parse(Configuration.values[:api_uri]).host,
        user: Configuration.values[:queue_user],
        password: Configuration.values[:queue_password],
        tls: Configuration.values[:enable_tls]
      ).tap(&:start)
    end

    def create_channel
      @channel = @server.create_channel(nil, Configuration.values[:worker_pool_size])
    end

    def create_queue(queue_name)
      @queue = @channel.queue queue_name, durable: true, auto_delete: true, no_declare: true
    end
  end
end
