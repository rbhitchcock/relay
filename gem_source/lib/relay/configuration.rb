require 'active_support/core_ext/hash'
require 'safe_yaml/load'

module Relay
  # Namespace for configuration settings
  module Configuration
    ::SafeYAML::OPTIONS[:default_mode] = :safe
    CONFIG_FILE = '/relay/configuration.yml'.freeze

    DEFAULT = {
      api_uri: 'https://norad.cisco.com:8443/v1',
      queue_user: 'norad-consumer',
      queue_password: '',
      db: '/relay/relay.store',
      private_key: '/relay/relay.pem',
      organization_token: '0',
      containers_file: '/relay/containers.yml',
      file_encryption_public_key_file: '/relay/file_encryption_key.pub',
      file_encryption_aes_key_file: '/relay/file_encryption_key.enc',
      worker_pool_size: 4,
      relay_host_docker_socket: 'unix:///relay/docker.sock',
      enable_tls: true
    }.freeze

    # Sets the configuration options to be used by the library
    def self.configure!
      @values = DEFAULT.merge(YAML.load(File.new(ENV['NORAD_RELAY_CONFIG_FILE'] || CONFIG_FILE)).symbolize_keys)
    end

    # @return [Hash] configuration options
    def self.values
      @values
    end
  end
end
