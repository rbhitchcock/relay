require 'openssl'
require 'securerandom'

module Relay
  # Crypto utility methods for encrypting the containers config file
  module CryptoUtilities
    AES_KEY_SIZE = 128

    class << self
      def encrypt_containers_file(api)
        return unless File.exist?(containers_file_path)
        public_key = save_public_key(api.fetch_details.fetch('public_file_encryption_key'))
        aes_key = encrypt_containers_file_with_aes
        encrypt_and_save_aes_key(aes_key, public_key)
        File.delete containers_file_path
      end

      def retrieve_aes_key(encoded_private_key)
        key = OpenSSL::PKey::RSA.new Base64.strict_decode64(encoded_private_key)
        key.private_decrypt(File.read(file_encryption_aes_key_file_path, mode: 'rb'))
      end

      private

      def encrypt_containers_file_with_aes
        cipher = OpenSSL::Cipher::AES.new(AES_KEY_SIZE, :CBC)
        cipher.encrypt
        aes_key = cipher.random_key # also sets the generated key on the Cipher
        aes_iv = cipher.random_iv # also sets the genetarted iv on the Cipher
        File.open("#{containers_file_path}.enc", 'w') do |f|
          f.write aes_iv + (cipher.update(File.read(containers_file_path)) + cipher.final)
        end
        aes_key
      end

      def save_public_key(encoded_key)
        existing_key = load_file_encryption_public_key
        new_key = OpenSSL::PKey::RSA.new Base64.strict_decode64(encoded_key)
        return existing_key if existing_key == new_key
        File.open(file_encryption_public_key_file_path, 'w') do |f|
          f.write new_key.public_key.to_pem
        end
        new_key
      end

      def encrypt_and_save_aes_key(aes_key, public_key)
        File.open(file_encryption_aes_key_file_path, 'w') do |f|
          f.write public_key.public_encrypt(aes_key)
        end
      end

      def load_file_encryption_public_key
        OpenSSL::PKey::RSA.new File.read(file_encryption_public_key_file_path)
      rescue StandardError
        nil
      end

      def file_encryption_aes_key_file_path
        Configuration.values[:file_encryption_aes_key_file]
      end

      def file_encryption_public_key_file_path
        Configuration.values[:file_encryption_public_key_file]
      end

      def containers_file_path
        Configuration.values[:containers_file]
      end
    end
  end
end
