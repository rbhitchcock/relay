require 'json'
require 'relay/logger'
require 'relay/amqp_connection'

module Relay
  # Listen on an AMQP queue for Docker commands
  class DockerQueue
    include Relay::Logger

    # Establishes a connection to an AMQP server
    #
    # @param queue_name [String] name of queue to connect to
    def initialize(queue_name)
      logger.info 'Info: Setting up remote management...'
      @connection = AmqpConnection.new queue_name
    end

    # Subscribe to an AMQP queue. This method blocks while it waits for messages on the queue
    def subscribe
      logger.info '[*] Waiting for commands on queue'
      @connection.queue.subscribe(block: true) do |_delivery_info, _properties, body|
        logger.debug "[x] #{body}"
        yield JSON.parse(body)
      end
    rescue SignalException => _
      @connection.close
    rescue JSON::ParserError
      logger.error 'Unable to parse message as JSON. Ignoring.'
    end

    # Closes the connection used by this queue
    def shutdown
      logger.info 'Shutting down amqp connection (no op)'
      @connection.close
    end
  end
end
