require 'log4r'

module Relay
  module Logger
    def self.included(base)
      class << base
        attr_writer :logger
        define_method :logger do
          return @logger if @logger
          @logger = Log4r::Logger.new 'norad'
          pf = { formatter: Log4r::PatternFormatter.new(pattern: "%d %l %C: #{name} %m") }
          @logger.outputters << Log4r::StdoutOutputter.new('norad', pf)
          @logger
        end
      end
    end

    def logger
      self.class.logger
    end
  end
end
