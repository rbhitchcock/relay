require 'json'
require 'relay/logger'

module Relay
  module MessageValidator
    include Relay::Logger

    IMAGE_REGEX = /\Anorad-registry\.cisco\.com:5000/

    class << self
      # Validates the message for expected options and format
      def valid?(msg)
        return false if msg.nil? || !msg.is_a?(Hash) || msg.empty?
        logger.debug "Validating msg: #{msg.inspect}"
        valid_docker_options?(msg['docker_options'])
      end

      private

      def valid_docker_options?(opts)
        opts && valid_image?(opts['Image']) && valid_cmd?(opts['Cmd']) && valid_env?(opts['Env'])
      end

      def valid_image?(image)
        image && image =~ IMAGE_REGEX
      end

      def valid_cmd?(cmd)
        !cmd.nil?
      end

      def valid_env?(env)
        !env.nil?
      end
    end
  end
end
