require 'rest-client'
require 'json'
require 'relay/logger'
require 'relay/api_payloads'
require 'relay/secure_communicator'
require 'relay/store'

module Relay
  # Provides an interface to the API that the Relay is communicating with
  class Api
    include Relay::Logger
    include Relay::ApiPayloads
    HEARTBEAT_INTERVAL = 300
    QUEUE_SLEEP = 5

    def initialize
      logger.info 'Creating client for communicating with API'
      @store = Store.new
      @site = RestClient::Resource.new Configuration.values[:api_uri]
      @secure_coms = SecureCommunicator.new
      @api_db_relay_id = @store.fetch_meta('api_db_relay_id')
      @api_db_relay_id.nil? ? register : heartbeat
      sleep QUEUE_SLEEP
      start_beating
    end

    # Registers the Relay with the API service
    # @return [True]
    # @raises [JSON::ParserError] or [RestClient::Exception] if unsuccessful
    def register
      resp = @site['/docker_relays'].post registration_payload(@secure_coms.encoded_public_key)
      update_relay_meta_info JSON.parse(resp.body)
      true
    rescue JSON::ParserError, RestClient::Exception => e
      logger.error "Registration error: #{e.message}"
      raise
    end

    # Sends a heartbeat message to the API service
    # @return [Boolean]
    def heartbeat
      logger.debug 'Sending heartbeat request to API'
      resp = signed_request :post, "/docker_relays/#{@api_db_relay_id}/heartbeat"
      update_relay_meta_info JSON.parse(resp.body)
    rescue JSON::ParserError, RestClient::Exception => e
      # FIXME: log more meaningfully
      logger.error "Heartbeat error #{e.message}"
      false
    end

    # Request detail about this relay from the API
    # @raises [KeyError], [JSON::ParserError] or [RestClient::Exception] if unsuccessful
    def fetch_details
      logger.debug 'Fetching details about this relay from the API...'
      resp = signed_request :get, "/docker_relays/#{@api_db_relay_id}"
      logger.debug 'Done'
      JSON.parse(resp.body).fetch('response')
    rescue KeyError, JSON::ParserError, RestClient::Exception => e
      logger.error "Unable to retrieve details for relay: #{e.message}"
      raise
    end

    def kill_heartbeat_thread
      @heartbeat_thread.kill
    end

    # @return [String] name of queue to connect to
    def queue_name
      @store.fetch_meta('queue_name')
    end

    private

    # FIXME: GET REQUEST
    def signed_request(verb, path, payload = {}, headers = {})
      payload.merge!(timestamp_payload)
      case verb
      when :get
        signed_get_request path, payload, headers
      when :post
        signed_post_request path, payload, headers
      end
    end

    def signed_post_request(path, payload = {}, headers = {})
      @site[path].post payload, headers.merge(sig_header(payload))
    end

    def signed_get_request(path, payload, headers = {})
      headers[:params] = payload
      @site[path].get headers.merge(sig_header(payload))
    end

    def sig_header(payload)
      { 'NORAD-SIGNATURE' => Base64.encode64(@secure_coms.sign(payload.to_json)).tr("\n", '') }
    end

    def heartbeat_every(n)
      loop do
        before = Time.now
        heartbeat
        interval = n - (Time.now - before)
        sleep interval if interval > 0
      end
    end

    def start_beating
      @heartbeat_thread = Thread.new do
        sleep HEARTBEAT_INTERVAL
        heartbeat_every(HEARTBEAT_INTERVAL)
      end
    end

    def update_relay_meta_info(j)
      return unless j['response']
      update_queue_name j['response']['queue_name']
      return unless j['response']['id']
      update_api_db_relay_id j['response']['id']
      @api_db_relay_id = j['response']['id']
    end

    [:queue_name, :api_db_relay_id].each do |m|
      define_method "update_#{m}".freeze do |val|
        @store.update_meta(m.to_s, val) if val
      end
    end
  end
end
