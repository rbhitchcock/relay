require 'openssl'
require 'base64'

module Relay
  class SecureCommunicator
    KEYSIZE = 4096

    def initialize
      # XXX: Safe to set this as an instance variable?
      @private_key = load_private_key
    end

    def sign(data, digest = OpenSSL::Digest::SHA256.new)
      @private_key.sign digest, data
    end

    def public_key
      @private_key.public_key
    end

    def encoded_public_key
      Base64.encode64(public_key.to_pem).tr("\n", '')
    end

    private

    # FIXME: Let's enforce some file permission check here
    def load_private_key
      OpenSSL::PKey::RSA.new File.read Configuration.values[:private_key]
    rescue Errno::ENOENT
      # File doesn't exist, so let's generate some new keys
      generate_keypair
    end

    def generate_keypair
      # FIXME: Should we do something to generate entropy?
      keypair = OpenSSL::PKey::RSA.new KEYSIZE
      save_keypair keypair
      keypair
    end

    def save_keypair(keypair)
      # Purposefully do not rescue from any file IO errors
      # FIXME enforce some permissions here
      File.open Configuration.values[:private_key], 'w' do |file|
        file.write keypair.to_pem
      end
    end
  end
end
