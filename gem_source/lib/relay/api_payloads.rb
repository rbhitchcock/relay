module Relay
  module ApiPayloads
    def registration_payload(public_key)
      {
        organization_token: Configuration.values[:organization_token],
        docker_relay: {
          public_key: public_key
        }
      }
    end

    def white_box_assessment_create_payload(id, command_id, category, name, description)
      {
        white_box_assessment: {
          identifier: id,
          title: name,
          description: description,
          category: category,
          command_id: command_id,
          command_type: 'AgentCommand'
        }
      }
    end

    def white_box_assessment_update_payload(state)
      {
        white_box_assessment: {
          state: state
        }
      }
    end

    def timestamp_payload
      { timestamp: Time.now.to_i.to_s }
    end

    def results_payload(results, ts = Time.now.to_i.to_s)
      { 'results[]' => results, :timestamp => ts }
    end

    def results_payload_for_signing(results, ts = Time.now.to_i.to_s)
      { results: results, timestamp: ts }
    end
  end
end
