#!/bin/sh

# Check for old docker-compose installs
if [ -e /usr/bin/docker-compose -a ! -e /usr/local/bin/docker-compose ] ; then
   ln -s /usr/bin/docker-compose /usr/local/bin/docker-compose
fi

# Check to see if norad-relay service installed
if [ -e /etc/systemd/system/norad-relay.service ] ; then
  systemctl stop norad-relay
fi

# Check to see if a configuration exists (save)
if [ -e /etc/norad.d/configuration.yml ] ; then
  cp /etc/norad.d/configuration.yml /etc/norad.d/configuration.yml.bk
fi
