#!/bin/sh

# Fixup the swarm init script
printf "\n\n\n=======================================================================================================================\n"
printf "  Before starting the relay, please open /etc/norad.d/configuration.yml in a text editor and follow the below instructions: \n"
printf "    * Replace #{ORG_TOKEN_VAR} with a valid ORG TOKEN!!!!!\n"
printf "    * Replace #{NORAD_URL_VAR} with either norad.cisco.com or an fqdn of a norad enterprise install!!!!\n"
printf "    * Note: Values should not be within a #{} (e.g. #{ORG_TOKEN_VALUE} is replaced with 9d00b762622abed8528ac3ec326289e8)\n"
printf "=======================================================================================================================\n\n\n"

# Restore previous configuration
if [ -e /etc/norad.d/configuration.yml.bk ] ; then
  mv /etc/norad.d/configuration.yml.bk /etc/norad.d/configuration.yml
fi

# Make sure root ownership
chown root:root /etc/norad.d
chmod 700 /etc/norad.d
chmod 600 /etc/norad.d/*
