FROM ruby:2.3.2

ENV RELAY_MODE=production

RUN touch ~/.gemrc && echo "gem: --no-ri --no-rdoc" >> ~/.gemrc
RUN gem install bundler -v 1.11.2

RUN mkdir -p /relay_build
WORKDIR /relay_build/
ADD gem_source/ /relay_build/
RUN bundle install --without development
RUN rake install

WORKDIR /
RUN rm -rf /relay_build

RUN mkdir -p /relay
RUN ln -s /relay /etc/norad.d

ENTRYPOINT ["relay"]
