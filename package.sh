#!/bin/bash
printf "Determining Distro\n"
printf "=================================\n"

# Grab the OS Distro
distro=`cat /etc/os-release | egrep '^NAME='`

printf "Building Package\n"
printf "=================================\n"

# Set the package version number
PKG_VER='1.0.2'

# Conditionals for OS
if [[ $distro =~ .*Ubuntu.* ]] ; then
   printf "Ubuntu Detected..\n"

   # Copy all the common files
   mkdir -p package/debian/src/etc/norad.d/
   cp -f package/common/* package/debian/src/etc/norad.d/

   # Source is a directory (-s), output is debian package, name is norad-relay
   fpm -s dir -t deb -n norad-relay -v ${PKG_VER} --deb-custom-control ./package/debian/DEBIAN/control --deb-templates ./package/debian/DEBIAN/templates  --deb-config ./package/debian/DEBIAN/config --before-install ./package/debian/DEBIAN/preinst --after-install ./package/debian/DEBIAN/postinst -C package/debian/src/

   mv norad-relay_${PKG_VER}_amd64.deb norad-relay-${PKG_VER}-amd64.deb
   printf "Final package created: norad-relay-${PKG_VER}-amd64.deb\n"

   # Cleanup
   rm -fr package/debian/src/etc/norad.d

elif [[ $distro =~ .*CentOS* ]] ; then
   printf "CentOS Detected..\n"

   # Copy all the common files
   mkdir -p package/centos/src/etc/norad.d/
   cp -f package/common/* package/centos/src/etc/norad.d/

   fpm -s dir -t rpm -n norad-relay --iteration '0' -v ${PKG_VER}  --before-install ./package/centos/scripts/preinst.sh --after-install ./package/centos/scripts/postinst.sh --rpm-os linux -C package/centos/src

   mv norad-relay-${PKG_VER}-0.x86_64.rpm norad-relay-${PKG_VER}-x86_64.rpm
   printf "Final package created: norad-relay-${PKG_VER}-x86_64.rpm\n"


   # Cleanup
   rm -fr package/centos/src/etc/norad.d
else
	printf "Unsupported Distro: ${distro}\n"
fi
